<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>Product</title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="container py-5">
                <div class="row">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('product')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Product</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card border-0 shadow">
                            <div class="card-header">
                                <h5 class="card-title">Product Form</h5>
                            </div>
                            <div class="card-body">
                                <form class="" action="{{route('product.store')}}" method="post">@csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label class="" for="name">Product Name</label>
                                            <input class="form-control" value="{{old('product_name')}}" placeholder="Product Name" name="product_name">
                                            @if($errors->has('product_name'))
                                                <small class="text-danger">{{ $errors->first('product_name') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label class="" for="name">Stock Quantity </label>
                                            <input class="form-control" type="number" value="{{old('quantity')}}" placeholder="Product Quantity" name="quantity">
                                            @if($errors->has('quantity'))
                                                <small class="text-danger">{{ $errors->first('quantity') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label class="" for="name">Unit Price </label>
                                            <input class="form-control" type="text" value="{{old('unit_price')}}" placeholder="Unit Price" name="unit_price">
                                            @if($errors->has('unit_price'))
                                                <small class="text-danger">{{ $errors->first('unit_price') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                           <button class="btn btn-sm btn-primary">Submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card border-0">
                            <div class="card-header">
                                <h5 class="card-title">Product List</h5>
                            </div>
                            <div class="table-responsive">
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Date</th>
                                    <th>Product Name</th>
                                    <th class="text-right">Quantity</th>
                                    <th class="text-right">Unit Price</th>
                                    <th class="text-right">Total Price</th>
                                    <th class="text-right">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $key=>$product)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$product['created_at']}}</td>
                                    <td>{{$product['product_name']}}</td>
                                    <td class="text-right">{{$product['quantity']}}</td>
                                    <td class="text-right">{{$product['unit_price']}}</td>
                                    <td class="text-right">{{$product['total_price']}}</td>
                                    <td class="text-right"><a href="{{route('product.edit',$product['id'])}}" class="btn btn-primary btn-sm">Edit</a></td>
                                </tr>
                                    @endforeach

                                </tbody>
                                <thead>
                                <tr>
                                    <th colspan="5">Total Sum</th>
                                    <th class="text-right">{{$products->sum('total_price')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
-->
</body>
</html>