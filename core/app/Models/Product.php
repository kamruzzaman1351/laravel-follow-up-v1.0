<?php

namespace App\Models;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class Product
{
    public function get():Collection{
        if(!Storage::disk('public')->exists('product.json')) {
            Storage::disk('public')->put('product.json', collect([])->toJson());
        }
        $file = Storage::disk('public')->path('product.json');
        $content = file_get_contents($file);
        $decode = json_decode($content,true);
       return collect($decode);
    }
    public function create(Collection $collection){
        Storage::disk('public')->put('product.json', $collection->toJson());
        return $collection;
    }
    public function update($id){

    }
    public function delete($id){

    }

}
