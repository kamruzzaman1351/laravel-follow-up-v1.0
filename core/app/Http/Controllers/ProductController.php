<?php

namespace App\Http\Controllers;


use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * @var Product
     */
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index()
    {
        $data['products'] = $this->product->get()->sortByDesc('created_at')->map(function ($item) {
            return [
                'id' => $item['id'],
                'product_name' => $item['product_name'],
                'quantity' => $item['quantity'],
                'unit_price' => $item['unit_price'],
                'total_price' => $item['quantity'] * $item['unit_price'],
                'created_at' => Carbon::parse($item['created_at'])->format('Y-m-d h:i:s'),
            ];
        })->values();
        return view('product', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required|string|max:191',
            'quantity' => 'required|integer|min:1',
            'unit_price' => 'required|numeric|min:0',
        ]);
        $products = $this->product->get();
        $id = 1;
        if ($max_id = $products->max('id')) {
            $id = $max_id + 1;
        }
        $products->push([
            'id' => $id,
            'product_name' => $request->product_name,
            'quantity' => $request->quantity,
            'unit_price' => $request->unit_price,
            'created_at' => Carbon::now(),
        ]);
        $products = $this->product->create($products);
        return redirect()->route('product');
    }

    public function edit($id)
    {
        $product = $this->product->get();
        if (!$product = $product->where('id', $id)->first()) {
            abort(404);
        }
        return view('edit_form', ['product' => $product]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_name' => 'required|string|max:191',
            'quantity' => 'required|integer|min:1',
            'unit_price' => 'required|numeric|min:0',
        ]);
        if (!$product = $this->product->get()->where('id', $id)->first()) {
            abort(404);
        }
        $products = $this->product->get()->map(function ($item) use ($request, $id) {
            if ($item['id'] == $id) {
                $item['product_name'] = $request->product_name;
                $item['quantity'] = $request->quantity;
                $item['unit_price'] = $request->unit_price;
            }
            return $item;
        });
        $this->product->create($products);
        return redirect()->route('product');
    }

}
